FROM openjdk:8
COPY code.java /tmp
WORKDIR /tmp
RUN ["javac", "code.java"]
ENTRYPOINT ["java", "first"]